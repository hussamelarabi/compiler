%option yylineno
%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "y.tab.h"
void yyerror(char * s);
%}

DIGIT [0-9]
ALPHA [a-z]
ALPHA_CAPITAL [A-Z]
ALNUM [0-9a-zA-Z]
ALLALPHA [a-zA-Z]


%%
"if"		return IF;
"else"		return ELSE;
"while"		return WHILE;
"for"		return FOR;
"switch"	return SWITCH;
"case"		return CASE;
"default"	return DEFAULT;
"do"		return DO;
"break"		return BREAK;

[\*\+\-\/\=\%\;\~\&\|\^\(\)\{\}\:]  return yytext[0];

"&&"		return AND;
"||"		return OR;
"!"			return NOT;
"=="		return EQ;
"!="		return NOTEQ;
">="		return GTE;
"<="		return LTE;
">"			return GT;
"<"			return LT;
"++"		return INC;
"--"		return DEC;



"int"		    return TYPE_INT;
"float"		    return TYPE_FLT;
"string"	    return TYPE_STRNG;
"char"		    return TYPE_CHR;
"const"		    return TYPE_CONST;
"bool"		    return TYPE_BOOL;
"exit"			return exit_command;
"symbol_table"  return show_symbol_table;
[ \t\n\r]+	;      



"false" 					{ yylval.INTGR = 0; return BOOL_VALUE; }
"true"  					{ yylval.INTGR = 1; return BOOL_VALUE; }
\-?{DIGIT}*"."{DIGIT}+      { yylval.FLT = atof(yytext); return FLOATING_NUM; }
\-?{DIGIT}+				 	{ yylval.INTGR = atoi(yytext); return NUM; }
\'.\'						{ yylval.INTGR = yytext[1]-'a'; return CHAR_VALUE; }
{ALLALPHA}{ALNUM}*			{ yylval.INTGR = *yytext-'a'; return ID; }
\".*\"        	            { yylval.STRNG = strdup(yytext); return STRING_VALUE; }								

"//".*              		;
[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]    ;

.							printf("Unexpected identifier, in line: %d, chars: %s\n", yylineno, yytext);

%%
