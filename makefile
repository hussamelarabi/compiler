run: a.out
	 ./a.out

a.out: yacc lex
	gcc lex.yy.c y.tab.c -ll
lex:
	lex lex.l
yacc: 
	yacc -d yacc.y

clean:
	rm y.tab.c lex.yy.c y.tab.h