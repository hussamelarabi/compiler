%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "boolean.h"

FILE *yyin;

int yylex(void);
void yyerror(char *);
extern int yylineno;

//Function Prototypes
void reset();
void declare_initalize(int id, int type_);
void declare_only(int id, int type_);
void assign_only(int id);
void declare_const(int id, int type);
void calc_lowp(char*);
void calc_highp(char*);
void cond_lowp(char*);
void cond_highp(char*);
void switch_test ();
void open_brace();
void close_brace ();
void switch_expr();
void print_symbol_table();
char * get_type(int type);
int new_scope();
int exit_scope();
//Declarations
int opened_scopes = 0;
int nesting_arr[100];
int nesting_last_index = -1;
int exitFlag=0;
int next_reg = 1; // The register number to be written in the next instruction
int next_cond_reg = 11;
int first_for = 1;
int is_first = 1; // check if is the first operation for consistent register counts
int is_first_cond = 1;
int after_hp = 0; // a high priority operation is done
int after_hp_cond = 0; // a high priority operation is done
int declared[26];
int is_constant[26];// for each variable store 1 if it constant
int scope[26]; // a scope number for each variable
int cscope = 0;
int next_case = 0;
int current_scope = 0;
int variable_initialized[26];
int type[26];

%}


%union {
	int INTGR; 
	char * STRNG; 
	float FLT; 
	char CHR;
	bool INTGR;
	}

%start statement
%token  IF ELSE FOR WHILE SWITCH CASE DO BREAK DEFAULT
%token  TYPE_INT TYPE_FLT TYPE_STRNG TYPE_CHR TYPE_CONST TYPE_BOOL
%token <INTGR> ID NUM BOOL_VALUE
%token <FLT> FLOATING_NUM 
%token <CHR> CHAR_VALUE 
%token <STRNG> STRING_VALUE 


%right '='
%left '&' '|' AND OR 
%left EQ NOTEQ GTE LTE GT LT
%left NOT '~' '^'
%left '+' '-' INC DEC
%left '*' '/'


%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE


 
%%
statement:
			  variable_declaration_statement ';' {reset();}
			| assign_statement ';'				 {reset();}
			| constant_declaration_statement ';' {reset();}
			| conditional_statement				 {reset();}
			| math_expr ';' 					 {reset();}
			| statement statement				 {reset();}
			| exit_command ';' 					 {exit(EXIT_SUCCESS);}
			| show_symbol_table ';' 			 {print_symbol_table();}
			;

conditional_statement:
              if_statement 		{;}
			| while_loop  	 	{;}
			| for_loop   		{;}
			| do_while  		{;}
			| switch_statement  {;}
			;

switch_statement:
			  SWITCH '(' math_expr ')' {switch_expr(); new_scope();}  '{' cases DEFAULT ':' statement {int tmp = exit_scope(); printf("label%d%c:\nlabel%d:\n",tmp,'a'+next_case-1,tmp);}  '}' 
			;

cases: 		 
			  CASE {if(next_case>0) {printf("label%d%c:\n",nesting_arr[nesting_last_index],'a'-1+next_case);} next_case++;} math_expr {switch_test();} ':' statement case_break {;}
			| cases cases {;}
			;
case_break:
			  /*epsilon*/ 
			| BREAK ';'{printf("JMP label%d\n",nesting_arr[nesting_last_index]);}
			;

do_while: 
			  DO '{' {printf("label:%d\n",new_scope()); open_brace();} statement '}' {close_brace();}  WHILE '(' condition ')'	{printf("JT R10,label%d\n",exit_scope());}
			;

for_loop:
			  FOR '(' start_assign_statement ';' {printf("MOV RF,0\n"); printf("label%d:\n",new_scope());reset();} condition ';' {printf("JF R10, label%da\n",nesting_arr[nesting_last_index]); printf("CMPE RF,0\n"); printf("JT R10, label%db\n", nesting_arr[nesting_last_index]);} assign_statement ')' '{' {printf("label%db:\n",nesting_arr[nesting_last_index]); printf("MOV RF,1\n"); open_brace(); reset();} statement '}' {printf("JMP label%d\n",nesting_arr[nesting_last_index]); printf("label%da:\n",exit_scope()); close_brace(); } {;}
			;

start_assign_statement: 
			  ID '=' NUM {assign_only($1);}
			| TYPE_INT ID '=' NUM {declare_initalize($2,1);}
			;

while_loop:
			  WHILE {printf("label%d:\n",new_scope());}  '(' condition ')' '{' {printf("JF R10, label%da\n",nesting_arr[nesting_last_index]);reset();open_brace();} statement '}' {printf("JMP label%d\n",nesting_arr[nesting_last_index]); printf("label%da:\n",exit_scope());reset();close_brace();} {;}
			;

if_statement:
			  IF '(' condition ')' '{' {printf("JF R10, label%d\n",new_scope());open_brace();reset();} statement '}' {printf("label%d:\n",exit_scope());close_brace();} {;} %prec LOWER_THAN_ELSE
			| IF '(' condition ')' '{' {printf("JF R10, label%d\n",new_scope());open_brace();reset();} statement '}' {printf("label%d:\n",exit_scope());close_brace();} ELSE '{' {printf("JT R10, label%d\n",new_scope());open_brace();reset();} statement '}' {;}
			;

condition:
			  '(' condition ')' {;}
			| condition OR  condition {;}
			| condition AND condition {;}
			| math_expr EQ math_expr	
			| math_expr NOTEQ math_expr
			| math_expr GTE math_expr	
			| math_expr GT math_expr	
			| math_expr LTE math_expr	
			| math_expr LT math_expr	
			| bool_element EQ bool_element
			| bool_element NOTEQ bool_element
			| bool_element 
			| NOT condition {printf("NOT R10\n");}
			;



math_expr:
			  '('math_expr')'
			| math_expr '+' math_expr
			| math_expr '-' math_expr
			| math_expr '*' math_expr
			| math_expr '/' math_expr
			| '~' math_expr			
			| math_expr '|' math_expr
			| math_expr '&' math_expr
			| math_expr '^' math_expr
			| math_element
			;

math_element:	
			  NUM 
			| FLOATING_NUM 
			| ID
			;

bool_element:
			  BOOL_VALUE 
			| ID
			| NOT ID
			;

assign_statement:
	 		  ID '=' math_expr
			;

variable_declaration_statement:
			   TYPE_INT ID  
			|  TYPE_FLT ID 
			|  TYPE_CHR ID 
			|  TYPE_STRNG ID
			|  TYPE_BOOL ID 
			|  TYPE_INT ID '=' math_expr 
			|  TYPE_FLT ID '=' math_expr 
			|  TYPE_CHR ID '=' CHAR_VALUE 
			|  TYPE_STRNG ID '=' STRING_VALUE
			|  TYPE_BOOL ID '=' bool_element 
			;

constant_declaration_statement:
			  TYPE_CONST TYPE_INT ID '=' math_expr 
			| TYPE_CONST TYPE_FLT ID '=' math_expr 
			| TYPE_CONST TYPE_CHR ID '=' CHAR_VALUE
			| TYPE_CONST TYPE_STRNG ID '=' STRING_VALUE
			| TYPE_CONST TYPE_BOOL ID '=' bool_element 
			;

%%

int main(int argc, char* args[]) {
   yyin = fopen("testfile.txt", "r+");
   if(yyin == NULL) {
	   printf("Error opening file");
   }
   else {
		printf(">>>>>>>>>>>>>>>>>>>>>>>>PARSING TOKENS<<<<<<<<<<<<<<<<<<<<<<<<\n");
		yyparse();
		printf(">>>>>>>>>>>>>>>>>>>>>>>>END OF PARSE<<<<<<<<<<<<<<<<<<<<<<<<\n");
   		fclose(yyin);
	}
   return 0;
}

void yyerror(char* s)
{
  fprintf(stderr, "%s in line %d \n",s, yylineno);
}
int yywrap()
{
  return(1);
}