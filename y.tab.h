/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IF = 258,
    ELSE = 259,
    FOR = 260,
    WHILE = 261,
    SWITCH = 262,
    CASE = 263,
    DO = 264,
    BREAK = 265,
    DEFAULT = 266,
    TYPE_INT = 267,
    TYPE_FLT = 268,
    TYPE_STRNG = 269,
    TYPE_CHR = 270,
    TYPE_CONST = 271,
    TYPE_BOOL = 272,
    ID = 273,
    NUM = 274,
    BOOL_VALUE = 275,
    FLOATING_NUM = 276,
    CHAR_VALUE = 277,
    STRING_VALUE = 278,
    AND = 279,
    OR = 280,
    EQ = 281,
    NOTEQ = 282,
    GTE = 283,
    LTE = 284,
    GT = 285,
    LT = 286,
    NOT = 287,
    INC = 288,
    DEC = 289,
    LOWER_THAN_ELSE = 290
  };
#endif
/* Tokens.  */
#define IF 258
#define ELSE 259
#define FOR 260
#define WHILE 261
#define SWITCH 262
#define CASE 263
#define DO 264
#define BREAK 265
#define DEFAULT 266
#define TYPE_INT 267
#define TYPE_FLT 268
#define TYPE_STRNG 269
#define TYPE_CHR 270
#define TYPE_CONST 271
#define TYPE_BOOL 272
#define ID 273
#define NUM 274
#define BOOL_VALUE 275
#define FLOATING_NUM 276
#define CHAR_VALUE 277
#define STRING_VALUE 278
#define AND 279
#define OR 280
#define EQ 281
#define NOTEQ 282
#define GTE 283
#define LTE 284
#define GT 285
#define LT 286
#define NOT 287
#define INC 288
#define DEC 289
#define LOWER_THAN_ELSE 290

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 15 "yacc.y"

	int INTGR; 
	char * STRNG; 
	float FLT; 
	char CHR;
	

#line 135 "y.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
